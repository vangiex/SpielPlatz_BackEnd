import subprocess , json , requests

def create(data):
    config = json.load(open('config.json', 'r')).get('DEFAULT')
    switch = json.load(open('config.json', 'r')).get('RESOURCE')
    url = json.load(open('config.json', 'r')).get('URL')
    
    _port = port()
    url = url['add'] + data['userid'] + "/" + data['name'] + "/" + data['os'] + "/" + switch['ip'] + "/" + str(_port) + "/" + config['Protocol']
    req = requests.get(url)

    cmd = "vboxmanage import " + switch[data['os']] + " && vboxmanage modifyvm 'Windows Xp' --name " + data['name'] + " && vboxmanage modifyvm " + data['name'] + " --vrdeport " + str(_port)    
    output = subprocess.check_output(cmd, shell=True)
    return output.decode()
    
def remove(data):
    cmd = "VBoxManage unregistervm " + data['name']
    output = subprocess.check_output(cmd, shell=True)
    return output.decode()

def start(data):
    cmd = "VBoxManage startvm " + data['name'] + " --type headless"
    output = subprocess.check_output(cmd, shell=True)
    return output.decode()

def stop(data):
    cmd = "VBoxManage controlvm " + data['name'] + "  poweroff --type headless"
    output = subprocess.check_output(cmd, shell=True)
    return output.decode()

def port():
    with open('config.json', 'r+') as f:
        pre = json.load(f)
        _port = pre['port']
        pre['port'] = _port + 1  
        f.seek(0)        
        json.dump(pre, f, indent=4)
        f.truncate()
    return _port

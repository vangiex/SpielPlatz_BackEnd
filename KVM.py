import subprocess , json , requests
import xml.etree.ElementTree as ET

def create(data):
    config = json.load(open('config.json', 'r')).get('DEFAULT')
    switch = json.load(open('config.json', 'r')).get('RESOURCE')
    url = json.load(open('config.json', 'r')).get('URL')
    
    _port = port()
    url = url['add'] + data['userid'] + "/" + data['name'] + "/" + data['os'] + "/" + switch['ip'] + "/" + str(_port) + "/" + config['Protocol']
    req = requests.get(url)

    cmd = "rar x /var/lib/libvirt/images/xp-kvm.rar /var/lib/libvirt/images/ && mv /var/lib/libvirt/images/xp.qcow2  /var/lib/libvirt/images/"+ data['name'] +".qcow2"
    print(cmd)
    output = subprocess.check_output(cmd, shell=True) 
    print(output)

    
    tree = ET.parse(switch[data['os']])
    root = tree.getroot()
    
    for i in range(len(root)):
        if root[i].tag == 'name':
            root[i].text = data['name']
            break
    
    root = tree.getiterator('devices')
    for i in range(len(root[0])):
            if root[0][i].tag == 'disk':
                root = root[0][i]
                break
    for i in root:
        if i.tag == 'source':
            root = i
            
    root.set('file', "/var/lib/libvirt/images/"+ data['name'] +".qcow2")
    
    root = tree.getiterator('devices')
    
    for i in range(len(root[0])):
            if root[0][i].tag == 'graphics':
                root = root[0][i]
                break
    root.set('port',str(_port))
    root.set('autoport','no')
    tree.write(data['name']+".xml")
    
    
  
    cmd = "virsh define "+ data['name'] +".xml && rm " + data['name'] +".xml"  
    output = subprocess.check_output(cmd, shell=True)
    
    return output.decode()
    
def remove(data):
    cmd = "virsh undefine " + data['name']
    output = subprocess.check_output(cmd, shell=True)
    return output.decode()

def start(data):
    cmd = "virsh start " + data['name']
    output = subprocess.check_output(cmd, shell=True)
    return output.decode()

def stop(data):
    cmd = "virsh shutdown " + data['name']
    output = subprocess.check_output(cmd, shell=True)
    return output.decode()

def port():
    with open('config.json', 'r+') as f:
        pre = json.load(f)
        _port = pre['port']
        pre['port'] = _port + 1  
        f.seek(0)        
        json.dump(pre, f, indent=4)
        f.truncate()
    return _port

import KVM as kvm , VB as vb
import json

def create(data):
    # log 
    
    config = json.load(open('config.json', 'r')).get('DEFAULT')
    
    output = vb.create(data)
    output = kvm.create(data)
    return output.decode()
    
def remove(data):
    #log
    
    config = json.load(open('config.json', 'r')).get('DEFAULT')
    
    output = vb.remove(data)
    output = kvm.remove(data)
    return output.decode()

def start(data):
    #log
    
    config = json.load(open('config.json', 'r')).get('DEFAULT')
    
    output = vb.start(data)
    output = kvm.start(data)
    return output.decode()

def stop(data):
    #log
    
    config = json.load(open('config.json', 'r')).get('DEFAULT')
    
    output = vb.stop(data)
    output = kvm.stop(data)
    return output.decode()


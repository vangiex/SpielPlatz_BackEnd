import socket , json , action , threading
from _thread import start_new_thread

lock = threading.Lock()
 
# thread fuction
def threaded(c):
    
    while True:
 
        # data received from client
        data = c.recv(1024)
        if not data:
            print('done')
            break
 
        # data function
        data = "{" + data.decode().split("{")[1]
        data = json.loads(data)
        print(data)
        lock.release()
        
        if(data['action'] == '0'):
            print(action.create(data))
        elif(data['action'] == '1'):
            print(action.start(data))
        elif(data['action'] == '2'):
            print(action.stop(data))
        elif(data['action'] == '3'):
            print(action.remove(data))
            
    # connection closed
    c.close()
 
 
def Main():
    
    config = json.load(open('config.json', 'r')).get('DEFAULT')

    host = config['Server']
    port = config['Port']
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    print("socket binded to {}:{}".format(host, port))
 
    # put the socket into listening mode
    s.listen(5)
    print("socket is listening")
    
    try:
        # a forever loop until client wants to exit
        while True:
     
            # establish connection with client
            c, addr = s.accept()
            # lock acquired by client
            lock.acquire()
            print('Connected to :', addr[0], ':', addr[1])
     
            # Start a new thread and return its identifier
            start_new_thread(threaded, (c,))
            
    except (KeyboardInterrupt, SystemExit):
        s.close()
        raise
    except:
        # report error and proceed
        print("error")
        
if __name__ == '__main__':
    Main()